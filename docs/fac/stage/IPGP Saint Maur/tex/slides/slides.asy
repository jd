if(!settings.multipleView)
 settings.batchView=false;
settings.texcommand="xelatex";
settings.tex="pdflatex";

// Beginning of Asymptote Figure 1
eval(quote{
defaultfilename='slides-1';
if(settings.render < 0) settings.render=4;
settings.inlineimage=true;
settings.embed=true;
settings.outformat='';
settings.toolbar=false;

draw((0,0)--(50,50)); draw((0,50)--(50,0)); \end {asy} \end {beamer@frameslide}\ifbeamer@twoscreenstext \beamer@dosecondscreennow {\begin {asy} draw((0,0)--(50,50)); draw((0,50)--(50,0)); \end {asy} }\fi \ifbeamer@anotherslide \advance \beamer@slideinframe by 1\relax \relax \expandafter \iterate \fi \let \iterate \relax \beamer@writeslidentry \beamer@reseteecodes 

\section{des choses et d'autres}
\subsection{des choses, d'abord}

\begin{frame}
\frametitle{Super important !}
\only<1,3-5>{
    \begin{itemize}[<+->]
    \item SM mesurée par susceptomètre MKF-B
    \item Valeurs relativement fortes : niveaux marneux (argiles paramagnétiques)
    \item Valeurs relativement faibles : niveaux calcaires (diamagnétiques).
    \item Les variations de la SM ne sont pas toujours anticorrélées à celles de la teneur en carbonates. Proposition : préservation de minéraux ferrimagnétiques dans certains niveaux calcaires ? Hypothèse à tester avec la minéralogie magnétique.
    \end{itemize}
}
\only<2>{
    \pifpaf{
        \begin{align}
        \rho \dt{v_i} & = \visc \laplacien \vv + \gradient \lp - \pression + \viscbulk \divergence \vv \rp + \rho \vg \label{eq:dynamique-jd} \notag
        \end{align}
    }
     \pifpaf{
        truc bidule\\
        yeah
    }
}
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}
\begin{frame}
    %\frametitle{}
    mouais
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}

\begin{frame}
    %\frametitle{}
    mouais
\end{frame}


\end{document}

