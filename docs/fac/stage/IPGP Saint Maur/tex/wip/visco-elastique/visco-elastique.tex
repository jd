\documentclass[12pt]{article}
\usepackage[fr, maths, tensoriel, matlab, natbib]{jd}
                                                       %%% DÉBUT DU DOCUMENT %%%

\begin{document}

\beq \boxed{\quad \rho \left ( \delt \vv + \vv \cdot \gradient \vv \right ) = \divergence \Tctr + \vf \quad} \label{eq:dynamique} \eeq

la forme locale de l'équation de la dynamique, avec $\vf$ la densité volumique des forces extérieures et $\Tctr$ le tenseur des contraintes. On a $tr(\Tctr) = -\bar \pression \cdot$ dim, avec dim = 3 en 3D ($\bar \pression$ est la pression thermodynamique, identifiée à la pression hydrostatique $\pression$ pour la plupart des cas d'étude). De ce fait, on peut écrire \beq \Tctrij = \Tctrijdev - \kro \Tctrmoyij \eeq en séparant la partie déviatorique de la partie moyenne (isotrope).
\plop
Soit $\vu$ le vecteur déplacement, \emph{i.e.} $\vu = \overrightarrow{XX'}$ avec $X$ et $X'$ les positions d'un même point avant et après déformation. On définit le tenseur des déformations $\Tdef$ (\emph{strain tensor}) comme la partie symétrique de la matrice du gradient de déplacement \beq \Tdefij = \Tdefexpansion \eeq On a $tr(\Tdef) = \kro \Tdefij = \frac{\Delta V}{V} = \divergence \vu$, et on définit $\Tdefmoy = \frac{tr(\Tdef)}{3}$. Dès lors \beq \Tdefijdev = \Tdefij - \Tdefmoy \eeq

Soit $\vv$ le vecteur vitesse. On définit le tenseur du taux de déformation $\Ttxdef$ (\emph{strain rate tensor}) comme la partie symétrique de la matrice du gradient de vitesse, \emph{i.e.} la dérivée temporelle de $\Tdef$ \beq \Ttxdefij = \Ttxdefexpansion \eeq On a $tr(\Ttxdef) = \kro \Ttxdefij = \divergence \vv$ et on définit $\Ttxdefmoy = \frac{tr(\Ttxdef)}{3}$. Dès lors \beq \Ttxdefijdev = \Ttxdefij - \Ttxdefmoy \eeq
\plop
En utilisant la même idée de séparation entre une partie moyenne isotrope et une partie déviatorique, on peut exprimer toute la complexité du lien (tenseur d'ordre 4) entre contrainte et déformation dans le cas isotrope au moyen de deux cœfficients : un pour la variation de volume, un pour le cisaillement.
\plop
La viscosité newtonienne isotrope est définie telle que
\begin{subnumcases}{\label{eq:visc}}
	\Tctrijdev & $=\quad 2 \visc ~   \Ttxdefijdev$ 			\label{eq:visc-un} \\
	\Tctrmoy   & $=\quad ~ \viscbulk \divergence \vv$	 	\label{eq:visc-deux}
\end{subnumcases}

et l'élasticité isotrope est définie telle que
\begin{subnumcases}{\label{eq:elas}}
	\Tctrijdev & $=\quad 2 G ~ \Tdefijdev$                    \label{eq:elas-un}\\
	\Tctrmoy   & $=\quad ~ K ~ \divergence \vu = -\pression$  \label{eq:elas-deux}
\end{subnumcases}

où $\visc$ est la viscosité dynamique (aussi dite primaire, en cisaillement…) et $\viscbulk$ la viscosité de volume (\emph{bulk viscosity}), en pascal seconde ; $G$ est le module de cisaillement, $K$ le module de compressibilité, en pascal (note : $\viscbulk$ est notée $K$ dans le pdf en italien sur la viscosité que tu m'as donné).
\plop

Pour les fluides, le modèle classique consiste à prendre Kelvin%
\footnote{%
	Modèle de Kelvin type « ressort et amortisseur en parallèle » :
	\begin{numcases}{}
		\Tdef^{\textrm{élastique}} 	& $=\quad \Tdef^{\textrm{visqueux}}$ \notag \\
		\Tctr 						& $=\quad \Tctr^{\textrm{élastique}} + \Tctr^{\textrm{visqueux}}$ \notag
	\end{numcases}
}
en volume ($\Tctrmoy$) et Maxwell%
\footnote{%
	Modèle de Maxwell type « ressort et amortisseur en série » :
	\begin{numcases}{}
		\Tctr^{\textrm{élastique}} 	& $=\quad \Tctr^{\textrm{visqueux}}$ \notag \\
		\Ttxdef 					& $=\quad \Tdef^{\textrm{élastique}} + \Tdef^{\textrm{visqueux}}$ \notag
	\end{numcases}
} en cisaillement ($\Ttxdefdev$), avec un module de cisaillement $G$ nul (modèle visco-élastique où la pression est la composante élastique). Si les forces volumiques dérivent d'un potentiel gravitationnel, \refeq{eq:dynamique} s'écrit
\begin{align}
\rho \dt{v_i}
  & = \deli{\Tctrij}{x_j} + \rho g_i \notag \\
  & = \divergence \Tctrdev + \divergence \underbrace{(- \pression + \viscbulk \divergence \vv)}_{\Tctrmoy} + \rho \vg \notag
  \intertext{et comme $G = 0$ pour les fluides, la partie déviatorique est purement visqueuse}
  & = \deli{2 \visc \Ttxdefijdev}{x_j} + \gradient \lp - \pression + \viscbulk \divergence \vv \rp + \rho \vg \notag \\
  \intertext{On montre que $2 \visc \divergence \Ttxdefdev = \visc \laplacien \vv$ (à $\visc$ constante !) et ce que l'hypothèse $\divergence \vv = 0$ soit appliquée ou non. De ce fait}
  \rho \dt{v_i} & = \visc \laplacien \vv + \gradient \lp - \pression + \viscbulk \divergence \vv \rp + \rho \vg \label{eq:dynamique-jd}
\end{align}

Une expression du $\Tctrij$ visco-élastique \emph{pour les fluides newtoniens isotropes} est également donnée par une loi de comportement de Hooke sous la forme
\beq \Tctrij = - \kro \pression + \lambda \kro \Ttxdefkk + 2 \visc \Ttxdefij \eeq
où interviennent les cœfficients de Lamé : $\visc$ caractérise la résistance au cisaillement et $\lambda$ la compressiblité (constants pour un milieu homogène). On peut en déduire que la contrainte moyenne normale totale (viscosité + élasticité) vaut
\beq \Tctrmoy = \frac{\Tctrkk}{3} = -\pression + \left ( \lambda + \frac{2}{3} \visc \right ) \Ttxdefkk \eeq
donc \beq \viscbulk = \viscbulkexpansion \eeq On peut également utiliser cette formule pour calculer d'une autre manière $\divergence \Tctrdev$ qui intervient dans la forme locale \refeq{eq:dynamique}.
\begin{align}
\divergence \Tctrdev
  & = \lambda \gradient ( \divergence \vv) + 2 \visc \divergence \Ttxdef \notag \\
  & = \lambda \gradient ( \divergence \vv) + 2 \visc \divergence \left ( \Ttxdefdev + \Ttxdefmoy \right ) \notag \\
  & = \lambda \gradient ( \divergence \vv) + 2 \visc \divergence \Ttxdefdev + \frac{2\visc}{3} \gradient ( \divergence \vv ) \notag \\
  & = \underbrace{\lp \lambda + \frac{2\visc}{3} \rp}_{\viscbulk} \gradient ( \divergence \vv) + \visc \laplacien \vv
  \intertext{en cohérence avec \refeq{eq:dynamique-jd}, mais on peut continuer le calcul}
  & = \lambda \gradient ( \divergence \vv) + \visc \laplacien \vv - \visc \gradient ( \divergence \vv ) + \frac{5\visc}{3} \gradient ( \divergence \vv ) \notag \\
  & = (\lambda + \frac{5\visc}{3} ) \gradient ( \divergence \vv) + \underbrace{\visc \laplacien \vv - \visc \gradient ( \divergence \vv )}_{- \visc \rotationnel(\rotationnel \vv)} \notag
\end{align}

Dans le cas d'un fluide de Stokes, on pose traditionnellement que $\viscbulk = 0 = \viscbulkexpansion$, ce qui permet de simplifier l'expression ci-dessus en
\beq (\lambda + \visc) \gradient ( \divergence \vv) - \visc \rotationnel(\rotationnel \vv) \label{eq:dynamique-lame} \eeq
En appliquant au-dessus de \refeq{eq:dynamique-jd} ou \refeq{eq:dynamique-lame} l'approximation de Boussinesq (impliquant $\divergence~\vv~=~0$), on se ramène à la forme classique
\beq \rho \dt{\vv} = \visc \laplacien \vv - \gradient \pression + \rho \vg  \eeq

\textbf{Mes problèmes/idées/réflexions/questions :)} \plop
Déjà, à ce stade, j'ai noté que quand on décompose la contrainte entre la partie isotrope $\pression$ et la partie déviatorique (viscosité), on a beau dire qu'on a un fluide visqueux, en fait il est visco-élastique :) On a juste caché dans $\pression$ les contraintes résultantes de la pression/compression isotrope. Ça me semble ok du fait qu'on applique une approximation anélastique qui filtre les ondes de compression/dépression, mais souvent c'est vendu comme allant de soi dans les bouquins que j'ai pu lire. C'est assez trompeur…
\plop
Un truc qui me gêne et avec lequel je n'arrive pas à m'accorder, compte-tenu de mes (faibles :)) connaissances actuelles. Annuler la viscosité de volume, c'est apparement faire $\viscbulk = 0 = \frac{2 \visc(1 + \nu)}{3(1 - 2\nu)}$ avec $\nu$ le cœfficient de Poisson ; donc $\nu$ est nécessairement égal à -1. Une telle valeur signifie qu'on estime que l'air est un milieu auxétique (il réduit en volume sous contrainte axiale en se contractant dans la direction perpendiculaire à la compression), ce qui me semble assez étrange comme hypothèse de travail :) Je veux dire, pourquoi pas, si c'est une approximation usuelle bien établie (je découvre), mais j'ai quand même essayé de réfléchir à la question d'un point de vue physique.
\plop
Dans mon esprit, si on suit un modèle « fluide de Stokes » mais sans le prendre au pied de la lettre, on cherche à dire que le travail des forces de viscosité est essentiellement lié au cisaillement (on mise sur $\visc$), pas aux variations de volume (on ne veut \emph{pas trop} de $\viscbulk$). C'est différent de dire que $\viscbulk = 0$ brutalement : ça signifie en fait qu'on vise une limite $\nu = 0$ (pas de variation de volume sous contrainte uniaxiale), auquel cas $\viscbulk = \frac{2\visc}{3}$ et on conserve une composante visqueuse volumique isotrope en $\viscbulk \divergence \vv$. Je ne suis personne pour remettre en question les approximations établies :) c'est juste que dans l'état de mes connaissances et en suivant « bêtement » mes calculs (que j'espère justes, mais…), quand je lis « $\viscbulk = 0$ définit le fluide de Stokes », j'arrive toujours à la conclusion que c'est bancal d'un point de vue physique (pas numérique…) et ce n'est pas le fait que $\viscbulk$ soit nul qui permet de négliger la viscosité de volume, mais la non-divergence sous l'approximation de Boussinesq (dans le cas présent). Dans le bouquin de Rieutord sur la dynamique des fluides, il est écrit : « en utilisant une approche statistique avec l'équation de Boltzman, on peut montrer que ce cœfficient [$\viscbulk$] est nul (du moins dans l'approximation de Boltzman) pour les gazs monoatomiques. Très souvent, on néglige purement et simplement $\viscbulk$ et lorsqu'on parle de la viscosité dynamique d'un fluide sans plus de précision, on entend $\visc$. Cette approximation qui consiste à négliger $\viscbulk$ s'appelle l'hypothèse de Stokes. On voit qu'en théorie elle ne convient bien qu'aux gazs monoatomiques. » Si tu as un avis sur la question, je suis preneur !
\plop
À partir de là, en me basant sur mes calculs rhéologiques :
\begin{itemize}
\item soit $\viscbulk \divergence \vv$ s'annule parce qu'on enchaîne sur l'approximation de Boussinesq qui impose $\divergence \vv = 0$, ce qui \emph{certes} revient analytiquement au même que d'avoir posé $\viscbulk = 0$ dès le début, mais à la mérite d'être cohérent avec un $\nu = 0$ plus « réaliste » qu'un cas auxétique (selon moi !) et évite de faire une hypothèse ou une autre sur la valeur de $\visc$ : ça n'impose que $\lambda = 0$, \emph{i.e.} l'incompressibilité. Ça à le mérite d'être assez cohérent ;
\item soit on peut aussi conserver cette composante en $\divergence \vv$, car j'ai cru comprendre qu'en numérique, avoir $\divergence \vv = 0$ est une gageure, du fait des erreurs d'arrondis. Ça peut alors servir de critère pour surveiller la divergence du modèle (dixit Laëtitia Le Pourhiet ^^) : tant que le $\viscbulk \divergence \vv$ « visqueux » reste petit, on est bon. Sinon, il faut mettre en place un \emph{penality factor} pour corriger l'algorithme en cours de route (je ne sais pas faire là comme ça, mais ça a l'air marrant :)). \plop
\end{itemize}

Au final, voilà ce que j'en ai conclu avant de t'écrire ce petit document, pour te demander ton avis éclairé :
\begin{itemize}
\item on souhaite éliminer la composante de viscosité de volume $\viscbulk$ et ne conserver que la viscosité en cisaillement $\visc$ ;
\item l'élasticité est en fait cachée dans la pression hydrostatique $P$, en cohérence avec…
\item …l'approximation de Boussinesq qui filtre les ondes sonores (anélasticité) et qui permet de justifier mathématiquement pourquoi on en arrive de toute manière à négliger la viscosité en volume de façon élégante ;
\item ça donne un fluide de Stokes au sens physique du terme, bien que je ne sois pas vraiment d'accord (mais ça vient peut-être de mon incompréhension totale du modèle !) avec l'assomption $\viscbulk = 0$ qui lui est associée dans la littérature (pour moi, ça donne un corps auxétique, bof). Par ailleurs, en faisant $\viscbulk = 0$, je ne trouve pas pareil que dans le texte sur la viscosité que tu m'as donné ($\frac{5}{3}$ au lieu de $\frac{4}{3}$), et je ne vois pas comment on peut trouver cette valeur sans choisir la valeur de $\lambda$, pas de $\viscbulk = \viscbulkexpansion$ ;
\item à $\visc$ ($==$ en cisaillement) constante, il ne reste qu'un simple laplacien de la vitesse dans les équations — et comme on a les valeurs numériques de la viscosité en utilisant un modèle d'atmosphère (USSA76, NRLMSISE-00...), je crois comprendre que ça ne doit pas être gênant de poser nos calculs à $\visc$ constante, car les pas d'intégration sont petits devant l'échelle spatiale de variation de $\visc$. Utiliser la forme en laplacien me semble dès lors plus sympa que la forme en rotationnel du rotationnel et gradient de la divergence, non ?
\item en terme de nombre de Deborah, on est au final dans une limite $De = 0$ qui correspond au cas uniquement visqueux, avec un temps de relaxation de la perturbation nul, sous les hypothèses faites (Stokes + Boussinesq).
\end{itemize}

\break

\textbf{Sur le propagateur visqueux}
\plop
Numériquement parlant, j'ai réécris le système d'équation en ajoutant la composante visqueuse sous la forme du laplacien. Il apparaît une dérivée seconde de la vitesse par rapport à z. Contrairement au cas sans viscosité, où toutes les dérivées premières des composantes constantes de la vitesse étaient advectées par une composante de la perturbation, cette dérivée seconde est autonome. Quand on passe en Fourier en cherchant les perturbations sous formes sinusoïdales, on se retrouve avec des dérivées secondes de $\uic$ agrémentées de facteurs $\neip$ (en ayant simplifié tous les $\eip$ des autres termes). Ça me semblait bizarre au premier abord, parce que je n'avais jamais eu ce cas auparavant, mais à la réflexion je me dis que c'est justement le facteur qui exprime une atténuation du phénomène sous l'effet de la viscosité, sur la partie non-perturbative (ce qui de proche en proche doit affecter les petites perturbations), non ? Je ne suis pas trop sûr de mon raisonnement :(
\plop
Système de départ en se basant sur les calculs ci-avant :
\begin{subnumcases}{\label{eq:systeme-IGW}(S)}
\delt \vv + \vv \cdot \gradient \vv = - \frac{1}{\rho} \gradient \pression + \vect g + \nu \laplacien \vv \\
\divergence \vv = 0 \\
\delt \rho + \divergence{\rho \vv} = 0
\end{subnumcases}

On décompose $X \in \{ \vv = (u, v, w), \rho, \pression \}$ en la somme d'une partie équilibre $X_0(z)$ et d'une (très petite devant 1 et devant la partie équilibre) perturbation $X_1(x,y,z,t)$. Par ailleurs, en suivant une analyse en ordre de grandeur, on décide de négliger $\uzc$. On applique l'approximation de Boussinesq ($\rho_0$ partout sauf sur le terme de flottabilité ; cela implique la non-divergence de la vitesse totale). Enfin, on linéarise au premier ordre (développements limités sur $\rho$ au premier ordre qui simplifient les termes de pression et de gravité, non prise en compte des termes quadratiques et au-delà, des produits de perturbations, etc.). Au final, on a :
\begin{subnumcases}{(S)}
\delt \uxp + \uxc \delx \uxp + \uyc \dely \uxp + \uzp \delz \uxc =
- \frac{1}{\rho_0} \delx \pp + \nu \left [ \left ( \delxs{} + \delys{} + \delzs{} \right ) \uxp + \dzs \uxc \right ] \notag \\
\delt \uyp + \uxc \delx \uyp + \uyc \dely \uyp + \uzp \delz \uyc =
- \frac{1}{\rho_0} \dely \pp + \nu \left [ \left ( \delxs{} + \delys{} + \delzs{} \right ) \uyp + \dzs \uyc \right ] \notag \\
\delt \uzp + \uxc \delx \uzp + \uyc \dely \uzp =
- \frac{1}{\rho_0} \delz \pp - \frac{\rho_1}{\rho_0} g + \nu \left [ \left ( \delxs{} + \delys{} + \delzs{} \right ) \uzp \right ] \notag \\
\delx \uxp + \dely \uyp + \delz \uzp = 0 \hfill \notag \\
\delt{\rho_1} + \uxc \delx{\rho_1} + \uyc \dely{\rho_1} + \uzp \delz{\rho_0} = 0 \notag
\end{subnumcases}

On cherche la solution de l'équation d'ondes en prenant $X_1(x,y,z,t) = \tilde X(z) {\mathrm e^{i(\overbrace{k_x x + k_y y - \omega t}^{\Phi})}}$ :
\begin{subnumcases}{(S)}
- i \omega \tux + \uxc i k_x \tux + \uyc i k_y \tux + \tuz \dz \uxc = - \frac{1}{\rho_0} i k_x \tp + \nu \left [ \left ( - k_x^2 - k_y^2 \right ) \tux + \dzs \tux + \neip \dzs \uxc \right ] \label{eq:igw-un} \\
- i \omega \tuy + \uxc i k_x \tuy + \uyc i k_y \tuy + \tuz \dz \uyc = - \frac{1}{\rho_0} i k_y \tp + \nu \left [ \left ( - k_x^2 - k_y^2 \right ) \tux + \dzs \tuy + \neip \dzs \uyc \right ] \label{eq:igw-deux} \\
- i \omega \tuz + \uxc i k_x \tuz + \uyc i k_y \tuz = - \frac{1}{\rho_0} \dz \tp - \frac{\trho}{\rho_0} g + \nu \left [ \left ( - k_x^2 - k_y^2 \right ) \tuz + \dzs \tuz \right ] \label{eq:igw-trois} \\
i k_x \tux + i k_y \tuy + \dz \tuz = 0 \label{eq:igw-quatre} \\
- i \omega \trho + \uxc i k_x \trho + \uyc i k_y \trho + \tuz \dz{\rho_0} = 0 \label{eq:igw-cinq}
\end{subnumcases}

Avant l'introduction de la viscosité, on avait un propagateur de la forme $\dz V = A V$ avec
$V = \begin{pmatrix}
\tuz^* \\
\tp^*
\end{pmatrix}$.

Dans l'équation \refeq{eq:igw-quatre}, on isole le $\dz \tuz$ et on exprime les autres termes avec \refeq{eq:igw-un} et \refeq{eq:igw-deux} (on peut noter $\Omega = \omega - \uxc k_x - \uyc k_y$). Ça donne un des deux termes de V. Désormais, il y a dans le membre de droite des trois premières équations du $\dzs \tui$. Avec \refeq{eq:igw-trois} et \refeq{eq:igw-cinq}, on construit $\dz \tp$ en fonction de $\dz \tuz$.
\plop
Dans l'idée, le système linéaire se résout pareil que dans le cas non-visqueux, en différences finies. Je pense qu'il est sage de les calculer centrées, aux deux ordres, mais faut voir pour les conditions initiales. \emph{Todo asap} :)

\clearpage

%\lstinputlisting{/home/jd/ninto/MATLAB/modesT-jd/ModesT-iso.m}

\bibliography{jd}

\end{document}

