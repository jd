% ----------------
% IGW: wind effect
% ----------------

%function modesT_vect(p_T, p_L, p_res, plotflags, exportflags)
function modesT_vect(varargin)

% arguments:
% - plotflags is [0|1, 0|1, 0|1, 0|1]
% - exportflags is [0|1, 0|1]
% plotflags are 0 by default, export_data is 1 (computations and export only)
% usage:
% - to generate data but no pics, modesT_vect(0, -50, (30:10:120), (50:50:400), 10, [0 0 0 0], [1 0]) eg.

% {{{ parameters handling and flags

optargin = size(varargin, 2);
stdargin = nargin - optargin;

if isa(varargin{1}, 'char')
    disp 'char'
    if varargin{1} == 'auto'
        auto_winds = 1;
    %elseif varargin{1} == 'ask'
        %auto_winds = 1;
        %% TODO uigetfile() to get wind models
        %% string length is an issue, may use cells instead?
        %error('not implemented yet!');
    else
        error('bad value for wind flag: must be ''auto'', ''ask'' or [u0b u0t]')
    end
else    % explicit winds
    auto_winds = 0;
    winds = varargin{1};
    %size(winds)
    if any((size(winds) == [1 2]) == 0)
        error('winds must be provided as one row arg: [u0b u0t] ');
    end
    u0b = winds(1)
    u0t = winds(2)
end

if nargin < 2
    p_T = [10, 15, 20, 25, 30, 45, 50].*60;
else
    p_T = varargin{2}.*60;
end
%p_T

do_auto_L = 0;
if nargin < 3
    p_L = [50 100 150 200 250 300 350 400].*1e3;
else
    if isa(varargin{3}, 'char')
        if varargin{3} == 'auto'
            do_auto_L = 1;
        else
            error('wrong value for wavelength (auto or an array)');
        end
    else
        p_L = varargin{3}.*1e3;
    end
end
%p_L

if nargin < 4
    p_res = 1
else
    p_res = varargin{4};
end

if nargin < 5
    plotflags = [0, 0, 0, 1];
end

if nargin < 6
    exportflags = [1, 0];
end

plot_cl          = plotflags(1);
plot_modes       = plotflags(2);
plot_amplitudes  = plotflags(3);
plot_scales      = plotflags(4);
do_export_data   = exportflags(1);
do_export_append = exportflags(2);

% parameter handling and flags }}}

% {{{ data sets

% modele1:
%   altitude (0 - 500km)
%   density
%   density derivative
%   log density derivative
% wind:
%   meridional and zonal
load modele1
if auto_winds
    load wind_Z
    load wind_M
end

res     = p_res;            % resolution: 1 by default, may be 10, 100... (diverges!)
binf    = 1;
bsup    = 9981;             % bsup for the wind data (499 km)
Z       = modele1(binf:res:bsup,1);
rho     = modele1(binf:res:bsup,2);
drho    = modele1(binf:res:bsup,3);
dlgrho  = modele1(binf:res:bsup,4);
n_mod   = size(Z,1);
n_mod2  = 2*n_mod;  % sparse matrix helper
I(1:n_mod) = 1;     % plot helper

dz = diff(Z).*1e3;
dz = [dz; dz(end)];

if auto_winds
    % winds data
    u0Z = wind_Z(:,2);  % zonal
    u0M = wind_M(:,2);  % meridional
    % space interp at modele1 res
    myu0Z = [];
    myu0M = [];
    for j = 2:size(u0Z)
        tmpZ = linspace(u0Z(j-1), u0Z(j), 21);
        tmpM = linspace(u0M(j-1), u0M(j), 21);
        myu0Z = [myu0Z tmpZ(1:end-1)]; % end-1 to prevent duplicate
        myu0M = [myu0M tmpM(1:end-1)];
    end
    myu0Z = [myu0Z tmpZ(end)];
    myu0M = [myu0M tmpM(end)];
    u0Z  = myu0Z';
    u0M  = myu0M';
    % mind the res!
    u0Z = -u0Z(binf:res:bsup);
    u0M = u0M(binf:res:bsup);
    %u0Z = zeros(n_mod, 1);
    %u0M = zeros(n_mod, 1);
    %size(u0M)
    %size(u0Z)
    %pause

    % related gradient
    alphaZ = [];
    alphaM = [];
    for j = 2:n_mod-1
        alphaZ = [alphaZ (u0Z(j+1) - u0Z(j-1))/(2*dz(j))];
        alphaM = [alphaM (u0M(j+1) - u0M(j-1))/(2*dz(j))];
    end
    alphaZ = [(u0Z(2)-u0Z(1))/dz(1) alphaZ (u0Z(end)-u0Z(end-1))/dz(end)]';
    alphaM = [(u0M(2)-u0M(1))/dz(1) alphaM (u0M(end)-u0M(end-1))/dz(end)]';
    alpha  = [alphaZ alphaM];
else
    alpha  = (u0t-u0b)/((Z(n_mod)-Z(1))*1.e3);
    u0     = Z.*(1.e3*alpha) + u0b;
    u0Z    = u0;
    u0M    = u0;
    alphaZ = alpha;
    alphaM = alpha;
end

% data sets }}}

% {{{ constants

M = 5.9768e+24; % Earth mass
G = 6.67e-11;   % G
R = 6378.e3;    % Earth radius
h = 2500;       % water depth
g = 9.8;        % g

% constants }}}

% {{{ modal pre-processing

T  = p_T       % tsunami-related period (forcing)
wn = 2*pi./T;   % induced frequency set

if do_auto_L
    c  = sqrt(g*h);
    kx = wn./c;
    ky = kx;
    L = round(2.*pi./kx.*1e-3)
else
    % mainly for test-purpose
    kx = 2*pi./p_L;
    ky = kx;
    L  = p_L./1e3
end
%pause

% modal processing }}}

% {{{ iterators

nlat = max(size(ky));
nlon = max(size(kx));
nt   = max(size(wn));

% iterators }}}

load BestView2

% {{{ loop over k
for ik = 1:nlon
    
    %ik
    ikx    = kx(ik);
    iky    = ky(ik);
    l = L(ik);
    k2     = ikx^2 + iky^2;
    kalpha = ikx.*alphaZ + iky.*alphaM;
    
    if plot_cl

        drawnow;        
        figure(2*ik);   
        set(2*ik, 'position', [44   250   500   750]);
        set(gcf, 'Color', 'w');
        set(gca, 'LineWidth', [2])
        set(gca, 'fontsize', 18);
        view(VV);
        title(['Hwater = ', num2str(h), 'm and kx = ',  num2str(ikx), ' rad/m'], 'fontsize', 18);
        %     xlabel('omega w (rad/s)', 'fontsize', 18);
        xlabel('period T (min)', 'fontsize', 18);
        ylabel('Vertical V_r_e_a_l (m/s)', 'fontsize', 18);
        zlabel('altitude (km)', 'fontsize', 18);
        grid on;
        hold on;

        figure(2*ik+1);
        set(2*ik+1, 'position', [44   250   500   750]);
        set(gcf, 'Color', 'w');
        set(gca, 'LineWidth', [2])
        set(gca, 'fontsize', 18);
        view(VV);
        title(['Hwater = ', num2str(h), 'm   and   kx = ',  num2str(ikx), ' rad/m'], 'fontsize', 18);
        %    xlabel('omega (rad/s)', 'fontsize', 18);
        xlabel('period T (min)', 'fontsize', 18);
        ylabel('Vertical V_i_m_a_g (m/s)', 'fontsize', 18);
        zlabel('altitude (km)', 'fontsize', 18);
        grid on;
        hold on;

    end

    % }}}
    
    % {{{ loop over frequencies
    %     with nt = max(size(wn))
    for iw = 1:nt %(nt-1)/2 + 2:nt
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % solving K*U = 0
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % {{{ constants, helpers

        N2       = -g.*dlgrho;
        N2over2g = (N2./(2*g)).^2;
        omega    = wn(iw);
        BigOmega = omega - (ikx.*u0Z + iky.*u0M);
        
        % constants, helpers }}}

        % {{{ non viscuous propagation
        
        c1 = -(-kalpha.*2.*dz./BigOmega + dlgrho.*dz);
        c2 = -i.*k2.*2.*dz./BigOmega;
        c3 = -(-dlgrho.*dz);
        c4 = -(i.*2.*dz.*(BigOmega + dlgrho.*g./BigOmega));
        
        % non viscuous propagation }}}
        
        % {{{ initial conditions
        
        % {{{ first step
        
        w(1) = complex(1,0);
        
        %kz2 = k2*(-g/omega/omega*dlgrho(1) - 1) - 0.25/6.4e7;
        kz2 = k2*(N2(1)./omega./omega - 1) - N2over2g;
        if N2(1) >= omega*omega
            % propagating wave
            p(1) = i/(k2)*(kalpha(1) - i*sqrt(kz2(1))*BigOmega(1) - 0.5*BigOmega(1)*dlgrho(1))*w(1);
        else
            p(1) = i/(k2)*(kalpha(1) -   sqrt(kz2(1))*BigOmega(1) - 0.5*BigOmega(1)*dlgrho(1))*w(1);
        end

        % first step }}}
        
        % {{{ second step

        b1 = kalpha(1)*dz(1) / BigOmega(1) - 0.5*dlgrho(1)*dz(1) - 1;
        b2 = i*k2*dz(1) / BigOmega(1);
        b3 = 0.5*dlgrho(1)*dz(1) - 1;
        b4 = -i*dz(1)*(BigOmega(1) + g*dlgrho(1)/BigOmega(1));

        % second step }}}

        % initial conditions }}}

        % 0 alternating rows to feed spdiags()
        % circular permutation for even indexes
        C13 = [c1'; c3'];
        C2  = [zeros(1, size(c2,1)); c2'];      % *
        C4  = [c4'; zeros(1, size(c4,1))];

        diagC13 = C13(:);
        diagC2  = C2(:);
        % sign issue on this one, needs a minus to get positive imaginary parts
        diagC4  = -C4(:);
        
        % initial conditions submatrix
        cl0 = [1 0; 0 1];
        cl1 = [b1 b2 1 0; b4 b3 0 1];
        cl  = [cl0 zeros(2, n_mod2-2) ; cl1 zeros(2, n_mod2-4)];

        % sparse coefficient matrix
        plop = ones(n_mod2, 1);
        preK = spdiags([-plop diagC4 diagC13 diagC2 plop], 0:4, n_mod2, n_mod2);
        preK = preK(1:n_mod2 - 4, :);
        K    = [cl; preK];
        %full(K)

        % RHS row
        R = [w(1); p(1); zeros(n_mod2-2, 1)];

        % solving (LU algo)
        U = K\R;

        % getting results
        w = U(1:2:end);
        p = U(2:2:end);
        
        % retrieving the other parameters
        ux = (-i.*alphaZ.*w + ikx.*p)./BigOmega;
        uy = (-i.*alphaM.*w + iky.*p)./BigOmega;
        rho1 = (-i.*dlgrho.*w)./BigOmega;
        %kz2 = k2.*((-g/omega/omega).*dlgrho-1) - 0.25/6.4e7;
        kz2 = k2.*(N2./omega./omega - 1) - N2over2g;
        
        % {{{ logs
        
        if do_export_data
            period = num2str(2*pi/omega/60);
            if auto_winds
                name   = ['data/wind/model/data_wind_L' num2str(l) '_T' period '.txt'];
            else
                name   = ['data/wind/data_wind' num2str(u0b) 'to' num2str(u0t) '_L' num2str(l) '_T' period '.txt'];
            end
            save_data = [real(w) imag(w) real(ux) imag(ux) real(uy) imag(uy) real(p) imag(p) real(rho1) imag(rho1)];
            if do_export_append
                save(name, 'save_data', '-ascii', '-append');
            else
                save(name, 'save_data', '-ascii');
            end
        end
        
        % logs }}}

        wmax(ik, iw)    = max(abs(real(w)));
        uxmax(ik, iw)   = max(abs(real(ux)));
        uymax(ik, iw)   = max(abs(real(uy)));
        pmax(ik, iw)    = max(abs(real(p)));
        rhomax(ik, iw)  = max(abs(real(rho1)));
        KZ(ik, iw, :)   = sqrt(kz2);

        if plot_modes

            figure(100);
            set(100, 'position', [10 1 750 600])
            set(gcf, 'Color', 'w');
            fnt   =  20;
            fnt2  =  18;
            Zmax  =  max(Z);
            Zmin  =  min(Z);

            subplot(1, 5, 1);
            plot(real(w), Z, 'r', 'linewidth', 2);
            hold on;
            plot(imag(w), Z, 'g', 'linewidth', 2);
            hold off;
            %axis([-2 2 0 max(Z)])
            v = axis;
            axis([v(1) v(2) Zmin Zmax])
            xlabel('Vertical V (m/s)', 'fontsize', 12);
            ylabel('Altitude (km)', 'fontsize', 16);
            title(['Period = ', period, ' mn' ' (omega = ', num2str(omega), ' rad/s)'], 'fontsize', 12);

            subplot(1, 5, 2);
            plot(real(ux), Z, 'r', 'linewidth', 2);
            hold on;
            plot(imag(ux), Z, 'g', 'linewidth', 2);
            hold off;
            %axis([-20 20 0 max(Z)])
            v = axis;
            axis([v(1) v(2) Zmin Zmax]);
            xlabel('Zonal V (m/s)', 'fontsize', 12);

            subplot(1, 5, 3);
            plot(real(uy), Z, 'r', 'linewidth', 2);
            hold on;
            plot(imag(uy), Z, 'g', 'linewidth', 2);
            hold off;
            %axis([-20 20 0 max(Z)])
            v = axis;
            axis([v(1) v(2) Zmin Zmax]);
            xlabel('Meridian V (m/s)', 'fontsize', 12);

            subplot(1, 5, 4);
            plot(real(p), Z, 'r', 'linewidth', 2);
            hold on;
            plot(imag(p), Z, 'g', 'linewidth', 2);
            hold off;
            %axis([-3000 3000 0 max(Z)])
            v = axis;
            axis([v(1) v(2) Zmin Zmax]);
            xlabel('Pressure (Pa)', 'fontsize', 12);
            title(['Lambda = ', num2str(l), ' km'  ' (kx = ',  num2str(ikx), ' rad/m)'], 'fontsize', 12);

            subplot(1, 5, 5);
            plot(real(rho1), Z, 'r', 'linewidth', 2);
            hold on;
            plot(imag(rho1), Z, 'g', 'linewidth', 2);
            hold off;
            %axis([-0.1 0.1 0 max(Z)])
            v = axis;
            axis([v(1) v(2) Zmin Zmax]);
            xlabel('Density (kg/m3)', 'fontsize', 12);

            figure(100);
            F = getframe(gcf);
            [X, Map]  =  frame2im(F);
            if auto_winds
                file  =  ['data/modes/mode_wauto_L' num2str(l) '_T_' period '.tif'];
            else
                file  =  ['data/modes/mode_w' num2str(u0b) 'to' num2str(u0t) '_L' num2str(l) '_T_' period '.tif'];
            end
            imwrite(X, file, 'tif', 'Compression', 'none');

            disp('PRESS ENTER TO CONTINUE...');
            %pause
            %close

            figure(300);
            subplot(1, 3, 1);
            plot(real(sqrt(kz2)), Z, 'r');
            hold on;
            plot(imag(sqrt(kz2)), Z, 'g');
            xlabel('kz (rad/m)', 'fontsize', 12);
            ylabel('Altitude (km)', 'fontsize', 16);
            hold off;
            subplot(1, 3, 2);
            plot(sqrt(N2), Z, 'r');
            hold on;
            v = axis;
            plot([omega omega], [v(3) v(4)], 'g');
            hold off;
            xlabel('N and omega (rad/s)', 'fontsize', 12);
            subplot(1, 3, 3);
            plot(2*pi./real(sqrt(kz2))*1.e-3, Z, 'r');
            hold on;
            plot(2*pi./imag(sqrt(kz2))*1.e-3, Z, 'g');
            hold off;
            xlabel('lambda Z (km)', 'fontsize', 12);

            %pause
            close
        end
        
        if plot_cl

            figure(2*ik);
            plot3(I*2*pi/omega/60, real(w), Z, 'k');
            plot3(I*2*pi/omega/60, imag(w), Z, 'r');

            figure(2*ik+1);
            plot3(I*2*pi/omega/60, imag(w), Z, 'k');

        end

        if plot_amplitudes
           
            figure(2*ik+2);
            hold on;
            plot(abs(w./w(1)), Z);
            F = getframe(gcf);
            [X, Map]  =  frame2im(F);
            file  =  ['amplitude_L' num2str(round(1/ikx*1.e-3)) '_T' num2str(2*pi/omega/60) '.tif'];
            imwrite(X, file, 'tif', 'Compression', 'none');
            hold off;

        end
        
        clear w ux uy p rho1 kz2
    end
end     % }}}

% essential user feedback :)
wmax
%pause

if plot_scales

    njump = 0;
    njump2 = size(wmax);

    n1 = 1;
    n2 = 1;

    h = figure;
    set(h, 'position', [2 700 800 600]);
    set(gcf, 'Color', 'w');
    font_size   =  22;
    font_sizeN  =  18;

    % how to plot scales?
    %flag_plot   =  input('-Plot omega-k or T-lambda ? (0 or 1) ');
    flag_plot = 1;
    colormap(hot);
    
    % vertical velocity perturbation
    %subplot(2, 3, 1);
    if flag_plot  ==  0
        pcolor(wn(n1:nt), kx(n2:nlon), log10(wmax(:,1+njump:njump2(2))));
    else
        pcolor(2*pi./wn(n1:nt)/60, 2*pi./kx(n2:nlon)*1.e-3, log10(wmax(:,1+njump:njump2(2))));
    end
    shading flat;
    colorbar;
    title('W_r_e_a_l', 'fontsize', font_size);
    if flag_plot  ==  0
        ylabel('kx (rad/m)', 'fontsize', font_size);
    else
        ylabel('lambda (km)', 'fontsize', font_size);
    end
    set(gca, 'LineWidth', [2])
    set(gca, 'fontsize', font_sizeN);

    %subplot(2, 3, 2);
    %if flag_plot  ==  0
        %pcolor(wn(n1:nt), kx(n2:nlon), log10(uxmax(:,1+njump:njump2(2))));
    %else
        %pcolor(2*pi./wn(n1:nt)/60, 2*pi./kx(n2:nlon)*1.e-3, log10(uxmax(:,1+njump:njump2(2))));
    %end
    %shading flat;
    %colorbar;
    %title('U_r_e_a_l', 'fontsize', font_size);
    %set(gca, 'LineWidth', [2])
    %set(gca, 'fontsize', font_sizeN);

    %subplot(2, 3, 3);
    %if flag_plot  ==  0
        %pcolor(wn(n1:nt), ky(n2:nlon), log10(uymax(:,1+njump:njump2(2))));
    %else
        %pcolor(2*pi./wn(n1:nt)/60, 2*pi./ky(n2:nlon)*1.e-3, log10(uymax(:,1+njump:njump2(2))));
    %end
    %shading flat;
    %colorbar;
    %title('U_r_e_a_l', 'fontsize', font_size);
    %set(gca, 'LineWidth', [2])
    %set(gca, 'fontsize', font_sizeN);

    %subplot(2, 3, 4);
    %if flag_plot  ==  0
        %pcolor(wn(n1:nt), kx(n2:nlon), log10(pmax(:,1+njump:njump2(2))));
    %else
        %pcolor(2*pi./wn(n1:nt)/60, 2*pi./kx(n2:nlon)*1.e-3, log10(pmax(:,1+njump:njump2(2))));
    %end
    %shading flat;
    %colorbar;
    %if flag_plot  ==  0
        %ylabel('kx (rad/m)', 'fontsize', font_size);
        %xlabel('omega (rad/s)', 'fontsize', font_size);
    %else
        %ylabel('lambda (km)', 'fontsize', font_size);
        %xlabel('period T (min)', 'fontsize', font_size);
    %end
    %title('P_r_e_a_l', 'fontsize', font_size);
    %set(gca, 'LineWidth', [2])
    %set(gca, 'fontsize', font_sizeN);

    %subplot(2, 3, 5);
    %if flag_plot ==  0
        %pcolor(wn(n1:nt), kx(n2:nlon), log10(rhomax(:,1+njump:njump2(2))));
    %else
        %pcolor(2*pi./wn(n1:nt)/60, 2*pi./kx(n2:nlon)*1.e-3, log10(rhomax(:,1+njump:njump2(2))));
    %end
    %shading flat;
    %colorbar;
    %if flag_plot  ==  0
        %xlabel('omega (rad/s)', 'fontsize', font_size);
    %else
        %xlabel('period T (min)', 'fontsize', font_size);
    %end
    %title('Rho_r_e_a_l', 'fontsize', font_size);
    %set(gca, 'LineWidth', [2])
    %set(gca, 'fontsize', font_sizeN);

end

