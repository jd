function [varargout] = data_processing(thefile, varargin)
% arguments:
% - file to process
% - what to get: real or imag part or both (say 'auto')
% - inferior limit
% - resolution
% - superior limit
% - 3D flag

msg = nargoutchk(4, 5, nargout);
if isempty(msg)
    optargin = size(varargin, 2);
    stdargin = nargin - optargin;

    data = load(thefile, '-ascii');

    binf = 1;
    res  = 1;
    bsup = size(data,1);

    threeD = 0;
    if optargin > 4
        if varargin{5} == '3D'
            threeD = 1;
        else
            error('last argument has bad value (try 3D or let empty)');
        end
    end

    if optargin > 0
        % real part, anyone?
        if varargin{1} == 'real'
            %thefile
            %size(data)
            w  = data(:,1);
            if threeD
                ux = data(:,3);
                uy = data(:,5);
                p  = data(:,7);
                r  = data(:,9);
            else
                u  = data(:,3);
                p  = data(:,5);
                r  = data(:,7);
            end
        elseif varargin{1} == 'imag'
            w  = data(:,2);
            if threeD
                ux = data(:,4);
                uy = data(:,6);
                p  = data(:,8);
                r  = data(:,10);
            else
                u  = data(:,4);
                p  = data(:,6);
                r  = data(:,8);
            end
        else
            w  = data(:,1) + i.*data(:,2);
            if threeD
                ux = data(:,3) + i.*data(:,4);
                uy = data(:,5) + i.*data(:,6);
                p  = data(:,7) + i.*data(:,8);
                r  = data(:,9) + i.*data(:,10);
            else
                ux = data(:,3) + i.*data(:,4);
                p  = data(:,5) + i.*data(:,6);
                r  = data(:,7) + i.*data(:,8);
            end
        end

        if ~isempty(varargin{2})
            binf = varargin{2};
        end

        if ~isempty(varargin{3})
            res = varargin{3};
        end

        if ~isempty(varargin{4})
            bsup = varargin{4};
        end
    else
        % on demand
        for k = 1:optargin
            % TODO
            disp 'ERROR: not implemented yet!'
        end
    end

    %varargout(k) = {s(k)}

    varargout(1)  = {w(binf:res:bsup)};
    if threeD
        varargout(2) = {ux(binf:res:bsup)};
        varargout(3) = {uy(binf:res:bsup)};
        varargout(4) = {p(binf:res:bsup)};
        varargout(5) = {r(binf:res:bsup)};
    else
        varargout(2)  = {u(binf:res:bsup)};
        varargout(3)  = {p(binf:res:bsup)};
        varargout(4)  = {r(binf:res:bsup)};
    end
else
    disp(msg)
end

