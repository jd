% -------------------------------
% IGW: main script (winds effect)
% -------------------------------

function wind_effect(varargin)

% TODO s'assurer de l'existence des dossiers, les creer sinon
% TODO demander les profils a utiliser (uigetfile)
% TODO indiquer la res dans les noms de fichiers, ca eviterait de les ouvrir :) regexp ^^
% TODO supprimer les output graphiques a chaque nouveau run ?

% {{{ parameters handling

optargin = size(varargin, 2);
stdargin = nargin - optargin;

for i = 1:nargin
    varargin{i}
end

if isa(varargin{1}, 'char')
    disp 'char'
    if nargin < 2
        warning('you may provide period(s), wavelenght(s) or both')
    end

    if varargin{1} == 'auto'
        auto_winds = 1;
    %elseif varargin{1} == 'ask'
        %auto_winds = 1;
        %% TODO uigetfile() to get wind models
        %% string length is an issue, may use cells instead?
        %error('not implemented yet!');
    else
        error('bad value for wind flag: must be ''auto'', ''ask'' or [u0b u0t]')
    end

else    % explicit winds
    auto_winds = 0;
    winds = varargin{1}
    size(winds)
    if any((size(winds) == [1 2]) == 0)
        error('winds must be provided as one row arg: [u0b u0t] ');
    end
    u0b = winds(1)
    u0t = winds(2)
end

if nargin < 2
    T = [15, 30, 60, 120];
else
    T = varargin{2};
end

if nargin < 3
    L  = [20 50 100 200 300 400];
else
    if isa(varargin{3}, 'char')
        if varargin{3} == 'auto'
            h  = 2500;
            g  = 9.8;
            c  = sqrt(g*h);
            wn = 2*pi./(T.*60);
            k  = wn./c;
            L  = round(2.*pi./k.*1e-3);
        else
            error('wrong value for wavelength(s) (must be ''auto'', a scalar or an array)');
        end
    else
        L = varargin{3};
    end
end
T
L
%pause
% parameters handling }}}

% {{{ flags and utils

if ~exist('force_compute', 'var')
    global force_compute
    force_compute = 1;  % 1 to force every computations, discarding checkings
end

check_res       = 0;    % 0 means assuming res == 1
plot_amplitudes = 0;
plot_pcolors    = 1;
threshold       = 5;    % dispersion to mean(mode(L/T)) (see below)
threshold2      = 3;    % number of authorized value(s) above threshold before yielding a local divergence error
                        % TODO make it depends on n_values
threshold3      = 100;  % max. abs amplitude not to overcome (global divergence error, at least 50% of the sets)

n_t             = length(T(:));
n_l             = length(L(:));
n_values        = n_t * n_l

% flags and utils }}}

prefix = 'data/';
res    = 1;     % be careful not to make test at low res, for the code diverges quickly
binf   = 1;
bsup   = 9981;  % for wind data sets are truncated
%bsup   = 2400;  % for wind data sets are truncated
omega  = 2.*pi./T;

if force_compute
    disp '--------------------------------------------------------------'
    disp 'force_compute is enabled: all data sets will be computed again'
    disp '--------------------------------------------------------------'
else
    disp '----------------------------------------------------------------------------------'
    disp 'force_compute is disabled: only outdated or unavailable data sets will be computed'
    disp '----------------------------------------------------------------------------------'
end
    
% {{{ USSA76 model

load modele1;
Z    = modele1(binf:res:bsup,1);
Zmax =  max(Z);
Zmin =  min(Z);

% USSA76 model }}}

% {{{ import no-wind values sets
% these are obtained by running modesT_vect while setting winds to 0 m/s

% {{{ getting refs

wind_ref = [];
do_L     = [];
do_T     = [];
do_check = [];

if force_compute
    do_L = L;
    do_T = T;
else
    for j = 1:length(L)
        for k = 1:length(T)
            do_it    = 0;
            name = [prefix 'wind/data_wind0to0_L' num2str(L(j)) '_T' num2str(T(k)) '.txt'];
            if exist(name, 'file')
                fprintf('%s %s %s %s', '"',  name, '"', ' exists');
                if check_res
                    fprintf('%s', ', checking its resolution...');
                    do_check = load(name);
                    % force computation if the provided file has wrong resolution
                    if size(do_check(:,1),1) ~= (bsup-binf/res+1)
                        fprintf('%s', ' must be computed again');
                        do_it = 1;
                    else
                        fprintf('%s', ' ok');
                    end
                    clear do_check;
                end
                fprintf('\n');
            else
                disp(['"' name '"' ' does not exists and will be computed'])
                do_it = 1;
            end

            if do_it
                do_L = [do_L L(j)];
                do_T = [do_T T(k)];
            end
        end
    end
    do_L = unique(do_L);
    do_T = unique(do_T);
end

if ~isempty(do_T) || ~isempty(do_L)
    fprintf('%s %d %s\n', '> computing required ref sets (', length(do_L(:)) * length(do_T(:)), ')');
    modesT_vect_wind([0, 0], do_T, do_L, res);  % outputs in data_wind0to0_L[L]_T[T].txt
end

fprintf('%s %d %s\n\n', '> importing reference sets (', n_values, ')');
for j = 1:length(L)
    fprintf('%d %s\t', L(j), ' km: ');
    for k = 1:length(T)
        name = [prefix 'wind/data_wind0to0_L' num2str(L(j)) '_T' num2str(T(k)) '.txt'];
        [w ux uy p r] = data_processing(name, 'real', binf, 1, bsup, '3D');
        wind_ref(:,1,j,k) = w;
        wind_ref(:,2,j,k) = ux;
        wind_ref(:,3,j,k) = uy;
        wind_ref(:,4,j,k) = p;
        wind_ref(:,5,j,k) = r;
        fprintf('%d ', T(k));
    end
    fprintf('\n')
end
fprintf('\n');

% getting refs }}}

%% plot checking
%for j = 1:length(L)
    %for k = 1:length(T)
        %figure(k);
        %set(k, 'position', [10 1 750 600]);
        %set(gcf, 'Color', 'w');

        %plot(wind_ref(:,1,j,k), Z, 'r', 'linewidth', 2);
        %v = axis;
        %axis([v(1) v(2) Zmin Zmax]);
        %xlabel('Vertical V (m/s)', 'fontsize', 12);
        %ylabel('Altitude (km)', 'fontsize', 16);
        %title(['Period = ', num2str(T), ' mn' ' (omega = ', num2str(omega), ' rad/s)'], 'fontsize', 12);
        %pause
    %end
%end

% }}}

% {{{ computations for the input parameters

% {{{ compute only required data files

if force_compute
    do_L = L;
    do_T = T;
else
    do_L     = [];
    do_T     = [];
    do_check = [];
    for j = 1:length(L)
        for k = 1:length(T)
            do_it = 0;
            name = [prefix 'wind/model/data_wind_L' num2str(L(j)) '_T' num2str(T(k)) '.txt'];
            if exist(name, 'file')
                fprintf('%s %s %s %s', '"',  name, '"', ' exists');
                if check_res
                    fprintf('%s', ', checking its resolution...');
                    do_check = load(name);
                    % force computation if the provided file has wrong resolution
                    if size(do_check(:,1),1) ~= (bsup-binf/res+1)
                        fprintf('%s', ' must be computed again');
                        do_it = 1;
                    else
                        fprintf('%s', ' ok');
                    end
                    clear do_check;
                end
                fprintf('\n');
            else
                disp(['"' name '"' ' does not exists and will be computed'])
                do_it = 1;
            end

            if do_it
                do_L = [do_L L(j)];
                do_T = [do_T T(k)];
            end
        end
    end
    do_L = unique(do_L);
    do_T = unique(do_T);
end

% compute only required data files }}}

if ~isempty(do_T) || ~isempty(do_L)
    fprintf('%s %d %s\n', 'computing required data sets (', length(do_L(:)) * length(do_T(:)), ')');
    if auto_winds
        disp '[auto winds computations]'
        modesT_vect_wind('auto', do_T, do_L, res);  % outputs in data_wind_L[L]_T[T].txt
    else
        disp '[explicit winds computations]'
        modesT_vect_wind([u0b, u0t], do_T, do_L, res);
    end
end

% computations }}}

% {{{ getting data

fprintf('%s %d %s\n\n', '> importing data sets (', n_values, ')');
data = [];
for j = 1:length(L)
    fprintf('%d %s\t', L(j), ' km: ');
    for k = 1:length(T)
        if auto_winds
            name = [prefix 'wind/model/data_wind_L' num2str(L(j)) '_T' num2str(T(k)) '.txt'];
        else
            %name = [prefix 'wind/data_wind_' 'L' num2str(L(j)) '_T' num2str(T(k)) '.txt'];
            name = [prefix 'wind/data_wind' num2str(u0b) 'to' num2str(u0t) '_L' num2str(L(j)) '_T' num2str(T(k)) '.txt'];
        end
        [w ux uy p r] = data_processing(name, 'real', binf, 1, bsup, '3D');
        data(:,1,j,k) = w; 
        data(:,2,j,k) = ux; 
        data(:,3,j,k) = uy; 
        data(:,4,j,k) = p; 
        data(:,5,j,k) = r; 
        fprintf('%d ', T(k));
    end
    fprintf('\n')
end
fprintf('\n')

% getting data }}}

% {{{ amplitudes processing

% dumb data sample:
%data(:,:,1,1) = [1 2; 3 4; 5 6];
%data(:,:,1,2) = [11 22; 33 44; 55 66];
%data(:,:,1,3) = [-111 -222; 333 444; 555 -666];
%data(:,:,2,1) = [101 202; 303 -404; 505 606];
%data(:,:,2,2) = [-1 -2; -3 -4; -5 -6];
%data(:,:,2,3) = [90 91; 92 93; 94 95];

disp '> processing amplitudes'

amp_max = [];
amp_ref = [];
%if ~auto_winds
    n_param = 5;
%else
    %n_param = 4;
%end

for x = 1:n_param
    amp_tmp          = squeeze(data(:,x,:,:));
    amp_tmp2         = squeeze(wind_ref(:,x,:,:));

    % 1. get max with respect to the signs
    %test  = squeeze(min(amp_tmp) +max(amp_tmp))  < 0;   % look for the sign
    %test2 = squeeze(min(amp_tmp2)+max(amp_tmp2)) < 0;
    %% then process each w record
    %for j = 1:size(test,1)
    %for k = 1:size(test,2)
    %if test(j,k) == 1
    %amp_max_builder = [amp_max_builder min(amp_tmp(:,j,k))];
    %else
    %amp_max_builder = [amp_max_builder max(amp_tmp(:,j,k))];
    %end

    %if test2(j,k) == 1
    %amp_ref_builder = [amp_ref_builder min(amp_tmp2(:,j,k))];
    %else
    %amp_ref_builder = [amp_ref_builder max(amp_tmp2(:,j,k))];
    %end
    %end
    %amp_max         = [amp_max; amp_max_builder];   % L in columns (one max per line for a given T),
    %amp_ref         = [amp_ref; amp_ref_builder];   % T in rows (one max per column for a given L)
    %amp_max_builder = [];
    %amp_ref_builder = [];
    %end

    % 2. get abs max
    amp_max(:,:,x)   = squeeze(max(abs(amp_tmp)));
    amp_ref(:,:,x)   = squeeze(max(abs(amp_tmp2)));

    % perform some divergence checks
    n_global_div_max = find(abs(amp_max(:,:,x)) > threshold3);
    n_global_div_ref = find(abs(amp_ref(:,:,x)) > threshold3);
    mean_max         = mean([mean(mode(amp_max(:,:,x))) mean(mode(amp_max(:,:,x)'))]);
    mean_ref         = mean([mean(mode(amp_ref(:,:,x))) mean(mode(amp_ref(:,:,x)'))]);
    amp_max_bad      = find(amp_max(:,:,x) > threshold * mean_max);
    amp_ref_bad      = find(amp_ref(:,:,x) > threshold * mean_ref);
    n_amp_max_bad    = length(amp_max_bad(:))
    n_amp_ref_bad    = length(amp_ref_bad(:))

    %if length(n_global_div_max(:)) > ceil(0.5*n_values) || length(n_global_div_ref(:)) > ceil(0.5*n_values)
        %error('global divergence!');    
    %end

    if floor(n_amp_max_bad + n_amp_ref_bad)/2 > threshold2
        error('local divergence!');
    end

    % get rid of aberrant values (yet actually no incidence on diff pcolor!)
    % may introduce some bias if mode is performed on L or T sets only,
    % so I took the mean of mean(mode(L)) and mean(mode(T))
    amp_max(amp_max_bad) = 0;
    amp_ref(amp_ref_bad) = 0;
    % in order to avoid quirks, let's be agressive on this:
    amp_max(amp_ref_bad) = 0;
    amp_ref(amp_max_bad) = 0;

    % feedback outputs
    %amp_ref
    %amp_max
    %amp_max-amp_ref
    %pause
end

% amplitudes processing }}}

% {{{ plot 'em all

if plot_amplitudes

    % FIXME outdated!
    for k = 1:length(T)
        figure(2*k);
        hold on;
        plot(L, amp_max(:,k), 'r', L, amp_ref(:,k), '--b');
        title(['maximum w amplitude vs wave length, for period ', num2str(T(k))]);
        legend_wind = ['wind (model)'];
        legend(legend_wind, 'no wind (ref)');
        hold off;

        F = getframe(gcf);
        [X, Map]  =  frame2im(F);
        file  =  ['data/output/amplitudes/model/by-period/amp_T' num2str(T(k)) '.tif'];
        imwrite(X, file, 'tif', 'Compression', 'none');
    end

    close all;

    for j = 1:length(L)
        figure(2*j+1);
        hold on;
        plot(T, amp_max(j,:), 'r', T, amp_ref(j,:), '--b');
        title(['maximum w amplitude vs period, for wave length ', num2str(L(j))]);
        legend_wind = ['wind (model)'];
        legend(legend_wind, 'no wind (ref)');
        hold off;

        F = getframe(gcf);
        [X, Map]  =  frame2im(F);
        file  =  ['data/output/amplitudes/model/by-wavelength/amp_L' num2str(L(j)) '.tif'];
        imwrite(X, file, 'tif', 'Compression', 'none');
    end

end

if plot_pcolors

    n1 = 1;
    n2 = 1;

    h = figure;
    set(h, 'position', [2 700 800 600]);
    set(gcf, 'Color', 'w');
    font_size  =  22;
    font_sizeN =  18;

    %flag_plot   =  input('-Plot omega-k or T-lambda ? (0 or 1) ');
    flag_plot = 1;
    colormap(hot);

    amp_max_w = amp_max(:,:,1)
    % FIXME 3D -> 2D mean...
    %if auto_winds
        amp_max_u = 0.5.*(amp_max(:,:,2) + amp_max(:,:,3));
        amp_max_p = amp_max(:,:,4);
        amp_max_r = amp_max(:,:,5);
    %else
        %amp_max_u = amp_max(:,:,2);
        %amp_max_p = amp_max(:,:,3);
        %amp_max_r = amp_max(:,:,4);
    %end
    amp_ref_w = amp_ref(:,:,1)
    %amp_ref_u = amp_ref(:,:,2);
    amp_ref_u = 0.5.*(amp_ref(:,:,2) + amp_ref(:,:,3));
    amp_ref_p = amp_ref(:,:,3);
    amp_ref_r = amp_ref(:,:,4);

    (amp_max_w - amp_ref_w)./amp_ref_w.*100
    pause
    
    % vertical speed disgression
    %subplot(2, 2, 1);
    if flag_plot  ==  0
        pcolor(wn, kx, (amp_max_w - amp_ref_w)./amp_ref_w.*100);
    else
        pcolor(T, L, (amp_max_w - amp_ref_w)./amp_ref_w.*100);
    end
    shading flat;
    colorbar;
    title('w disgression (%) from no-wind ref.', 'fontsize', font_size);
    if flag_plot  ==  0
        xlabel('omega (rad/s)', 'fontsize', font_size);
        ylabel('kx (rad/m)', 'fontsize', font_size);
    else
        xlabel('period T (min)', 'fontsize', font_size);
        ylabel('wavelength L (km)', 'fontsize', font_size);
    end
    set(gca, 'LineWidth', [2])
    set(gca, 'fontsize', font_sizeN);

    %%horizontal speed disgression
    %subplot(2, 2, 2);
    %if flag_plot  ==  0
        %pcolor(wn, kx, (amp_max_u - amp_ref_u)./amp_ref_u.*100);
    %else
        %pcolor(T, L, (amp_max_u - amp_ref_u)./amp_ref_u.*100);
    %end
    %shading flat;
    %colorbar;
    %title('u disgression (%) from no-wind ref.', 'fontsize', font_size);
    %if flag_plot  ==  0
        %xlabel('omega (rad/s)', 'fontsize', font_size);
        %ylabel('kx (rad/m)', 'fontsize', font_size);
    %else
        %xlabel('period T (min)', 'fontsize', font_size);
        %ylabel('wavelength L (km)', 'fontsize', font_size);
    %end
    %set(gca, 'LineWidth', [2])
    %set(gca, 'fontsize', font_sizeN);
    
    %%pression disgression
    %subplot(2, 2, 3);
    %if flag_plot  ==  0
        %pcolor(wn, kx, (amp_max_p - amp_ref_p)./amp_ref_p.*100);
    %else
        %pcolor(T, L, (amp_max_p - amp_ref_p)./amp_ref_p.*100);
    %end
    %shading flat;
    %colorbar;
    %title('p disgression (%) from no-wind ref.', 'fontsize', font_size);
    %if flag_plot  ==  0
        %xlabel('omega (rad/s)', 'fontsize', font_size);
        %ylabel('kx (rad/m)', 'fontsize', font_size);
    %else
        %xlabel('period T (min)', 'fontsize', font_size);
        %ylabel('wavelength L (km)', 'fontsize', font_size);
    %end
    %set(gca, 'LineWidth', [2])
    %set(gca, 'fontsize', font_sizeN);
    
    %%rho disgression
    %subplot(2, 2, 4);
    %if flag_plot  ==  0
        %pcolor(wn, kx, (amp_max_r - amp_ref_r)./amp_ref_r.*100);
    %else
        %pcolor(T, L, (amp_max_r - amp_ref_r)./amp_ref_r.*100);
    %end
    %shading flat;
    %colorbar;
    %title('rho disgression (%) from no-wind ref.', 'fontsize', font_size);
    %if flag_plot  ==  0
        %xlabel('omega (rad/s)', 'fontsize', font_size);
        %ylabel('kx (rad/m)', 'fontsize', font_size);
    %else
        %xlabel('period T (min)', 'fontsize', font_size);
        %ylabel('wavelength L (km)', 'fontsize', font_size);
    %end
    %set(gca, 'LineWidth', [2])
    %set(gca, 'fontsize', font_sizeN);
end

% plot 'em all }}}

% }}}

